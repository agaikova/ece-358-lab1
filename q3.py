import q2
import argparse
import os
import matplotlib as mpl
if os.environ.get('DISPLAY', '') == '':
    print('No display found; using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot
import numpy as np


def xrange(start, stop, step = 1.0):
    count = 0
    while True:
        tmp = float(start + count*step)
        if tmp >= stop:
            break
        yield tmp
        count +=1


if __name__ == "__main__": 
    parser = argparse.ArgumentParser()
    parser.add_argument("-time", "-T", type=float, default=1000, help="Time period to generate for")
    parser.add_argument("-lowerLim", "-ll", type=float, default=0.25, help="Lower limit for rho")
    parser.add_argument("-upperLim", "-ul", type=float, default=0.95, help="Upper limit fro rho")
    parser.add_argument("-stepSize", "-ss", type=float, default=0.1,  help="Step size for rho sweep")
    args = parser.parse_args()

    L = 2000
    C = 1000000 # 1 Mbps
    rho = args # arrival rate / avg service rate -> avg service rate should be averahe 

    x_axis = np.arange(args.lowerLim, args.upperLim, args.stepSize)
    all_E_N = []
    all_P_idle = []
    for rho in xrange(args.lowerLim, args.upperLim, args.stepSize):
        lambd = C*rho/L
        print("lambda is "+ str(lambd))
        print("rho is "+ str(rho))
        E_N, P_idle = q2.main(args.time, lambd, L, C)
        all_E_N.append(E_N)
        all_P_idle.append(P_idle)

    
    mpl.pyplot.plot(x_axis, all_E_N)
    mpl.pyplot.xlabel("rho")    
    mpl.pyplot.ylabel("Time-average")
    mpl.pyplot.title("Time-average of Number of Packets in queue by varrying rho")
    mpl.pyplot.grid(color='black', linestyle='--', linewidth=0.5)
    mpl.pyplot.savefig('E_N.png')
    mpl.pyplot.close()


    mpl.pyplot.plot(x_axis, all_P_idle)
    mpl.pyplot.xlabel("rho")    
    mpl.pyplot.ylabel("P_idle")    
    mpl.pyplot.title("Percentage of time queue spends idle")    
    mpl.pyplot.grid(color='black', linestyle='--', linewidth=0.5)
    mpl.pyplot.savefig('P_idle.png')
    mpl.pyplot.close()
         
