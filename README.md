This is Stacy and Joseph's ECE 358 Project

Thank you for taking a look.
:)

# Running this code
To run the programs use 
`python3 qx.py`
All programs have the assignment question values set as defaults, so they can all be run without optional arguments.

For q2.py you can set the time using the -T parameter and the rho value using the -R parameter. An example command is
`python3 q2.py -R 0.25 -T 1000`
 
For q3.py you can set the lower limit using -ll, the upper limit of rho using -ul, the step size using -ss, and the time using -T. 
An example command is
`python3 q3.py -ll 0.25 -ul 1.00 -ss 0.1 -T 1000`

For q5.py you can set the time using the -T param the rho using the -r command and the queue size with -q
An example command is
`python3 q5.py -T 1000 -r 1.5 -q 25`

Each of these scripts relies on a custom queue implementation, which can be found in the file queue.py

