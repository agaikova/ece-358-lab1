import random
import argparse
import numpy as np
import queue
import q1
import sys

lambd_multiplier = 5.5

np.random.seed(5)


def gen_rand(lambd):
    return -(1/lambd)*np.log(1-random.random())

# Fills the event queue with the arrival and the observal events
def prep_queue(T, lambd):
    event_q = queue.Queue()
    currTime = 0
    arrival_previous = 0
    observer_previous = 0
  
    # arrival time:
    while (currTime < T):
        arrival_time = currTime + gen_rand(lambd)
        event_q.enqueue(("a", arrival_time))
        currTime = arrival_time

    currTime = 0
    while (currTime < T):
        observal_time = currTime + gen_rand(lambd*lambd_multiplier)
        event_q.enqueue(("o", observal_time))
        currTime = observal_time

    return event_q

def main(T, queueSize, L, C, lambd = 45):
    numArrives = 0
    numDeparts = 0
    numObservs = 0

    TotalPacketCounter = 0
    IdleCounter = 0
    droppedPacketCounter = 0

    observer_packets_in_queue = []

    event_q  = prep_queue(T, lambd)
    event_q.sort()

    #queue used to store departure events waiting to leave
    departure_q = queue.Queue(queueSize)

    while not event_q.isEmpty():
        curr_event = event_q.dequeue()

        #Will check if the queue is full
        #Creates departure event if not otherwise packet dropped
        if (curr_event[0] == 'a'):
            if(not departure_q.isFull()):
                arrival_time = curr_event[1]
                numArrives +=1
                
                # Generates service time as a function of L and C
                # if there are no events waiting in queue will set its depart time as the arrival time plus the service
                # otherwise the packet will need to wait until all packets infront are processed so its depart time
                # will be the last ones depart time plus its service time
                service_time = gen_rand(1/L)/C
                depart_time = arrival_time + service_time if ( departure_q.isEmpty() ) else departure_q.items[0][1] + service_time
                depart_event = ("d", depart_time)
                departure_q.enqueue(depart_event)
                departure_q.sort()
                #inserts the event into the proper spot instead of sorting as that adds a lot to execution time
                event_q.insert(depart_event)

            else:
                droppedPacketCounter += 1

        elif (curr_event[0] == 'd'):
            numDeparts +=1
            departure_q.dequeue()


        elif (curr_event[0] == 'o'):
            numObservs += 1
            # find time-average of the number of packets in the queue E[N]
            TotalPacketCounter = numArrives - numDeparts
            observer_packets_in_queue.append(TotalPacketCounter)
            if (TotalPacketCounter == 0):
                IdleCounter += 1

    E_N = sum(observer_packets_in_queue)/len(observer_packets_in_queue)
    P_idle = str(IdleCounter/numObservs)
    P_loss = (droppedPacketCounter / (numArrives + droppedPacketCounter))*100
    # time avg of the num of packets in queue - E[N] 
    print("Time-Avg of number of queue packets: " + str(sum(observer_packets_in_queue)/len(observer_packets_in_queue)))
    print("Percent of time spent idle: " + str(IdleCounter/numObservs))
    print ("Percent of lossed packets: " + str(P_loss))
    print("arr dep obs loss")
    print(numArrives, numDeparts, numObservs, droppedPacketCounter)
    return E_N, P_idle, P_loss

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-time", "-T", type=float, default=1, help="Time period to generate for")
    parser.add_argument("-queueSize", "-q", type=int, default=10, help="Size of the queue")
    parser.add_argument("-rho", "-r", type=float, default=0.5, help="The rho which is the arrival rate over the service rate")

    args = parser.parse_args()
    L = 2000
    C = 1000000 # 1 Mbps
    rho = (args.rho) # arrival rate / avg service rate -> avg service rate should be averahe 

    lambd = C*rho/L
    main(args.time, args.queueSize, L, C, lambd)