import random
import argparse
import numpy as np
import queue

lambd_multiplier = 5.0

np.random.seed(5)

def gen_rand(lambd):
    return -(1/lambd)*np.log(1-random.random())

def prep_queue(T, lambd, L, C):
    event_q = queue.Queue()
    outstanding_dep = queue.Queue()
    currTime = 0
    departure_previous = 0 # alias as sum of outstanding items in the arrival queue at the time that a new event is generated
  
    # arrival time:
    while (currTime < T):

        arrival_time = currTime + gen_rand(lambd)
        service_time = gen_rand(1/L)/C

        # if currTime > previously calculated departure time, we should enqueue a departure 
        while (not outstanding_dep.isEmpty() and outstanding_dep.items[-1][1] < arrival_time):
            tup = outstanding_dep.dequeue()
            event_q.enqueue(tup)

        departure_time = arrival_time + service_time if (event_q.numArr == 0) else departure_previous + service_time
        outstanding_dep.enqueue(('d', departure_time))
        outstanding_dep.sort()

        event_q.enqueue(("a", arrival_time))
        departure_previous = departure_time
        currTime = arrival_time

    while not outstanding_dep.isEmpty():
        event_q.enqueue(outstanding_dep.dequeue())        

    currTime = 0
    while (currTime < T):
        observal_time = currTime + gen_rand(lambd*lambd_multiplier)
        event_q.enqueue(("o", observal_time))
        currTime = observal_time

    return event_q


def main(T, lambd, L, C):
    numArrives = 0
    numDeparts = 0
    numObservs = 0

    TotalPacketCounter = 0
    IdleCounter = 0

    observer_packets_in_queue = []

    event_q  = prep_queue(T, lambd, L, C)
    event_q.sort()

    while not event_q.isEmpty():
        curr_event = event_q.dequeue()

        if (curr_event[0] == 'a'):
  #          print("arrival at time "+str(curr_event[1]))
            numArrives +=1 
        if (curr_event[0] == 'd'):
 #           print("depart at time "+str(curr_event[1]))
            numDeparts +=1
        if (curr_event[0] == 'o'):
 #           print("observe at time "+str(curr_event[1]))
            numObservs += 1

            # find time-average of the number of packets in the queue E[N]
            TotalPacketCounter = numArrives - numDeparts
            observer_packets_in_queue.append(TotalPacketCounter)
            if (TotalPacketCounter == 0):
                IdleCounter += 1

    E_N = sum(observer_packets_in_queue)/len(observer_packets_in_queue)
    P_idle = float(IdleCounter/numObservs)
    # time avg of the num of packets in queue - E[N] 
    print("Time-Avg of number of queue packets: " + str(sum(observer_packets_in_queue)/len(observer_packets_in_queue)))
    print("Percent of time spent idle: " + str(IdleCounter/numObservs))
    return E_N, P_idle

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-time", "-T", type=float, default=1000, help="Time period to generate for")
    parser.add_argument("-rho" , "-R", type=float, default=0.25, help="Rho - arrival rate / average service rate")
    args = parser.parse_args()

    L = 2000
    C = 1000000 # 1 Mbps
    rho = args.rho # arrival rate / avg service rate 

    lambd = C*rho/L
    print("lambda is " + str(lambd))
    main(args.time, lambd, L, C)

