#!/usr/bin/python3
import random
import numpy as np
import os
import matplotlib as mpl
if os.environ.get('DISPLAY', '') == '':
    print('No display found; using non-interactive Agg backend')
    mpl.use('Agg')
import matplotlib.pyplot
# numpy random uniform

np.random.seed(5);

def main(num_samples, lambd = 75):
    y = []

    for i in range(num_samples):
        res = -(1/lambd)*np.log(1-random.random())
        y.append(res)
    return y

def plot_func(y):
    print("Mean of the exponential data is: " + str(np.mean(y)))
    print("Variance of the exponential data is " + str(np.var(y)))
    mpl.pyplot.plot(np.sort(y))
    mpl.pyplot.savefig('normal_distribution.png')

if __name__ == '__main__':
    plot_func(main(1000)) # default lambda is 75


