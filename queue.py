class Queue:
    def __init__(self, maxSize = float("inf")):
        self.items = []
        self.maxSize = maxSize
        self.numArr = 0

    def isEmpty(self):
        return self.items == []

    def isFull(self):
        return len(self.items) == self.maxSize

    def insert(self, event):
        # Goes through the items starting with the oldest to see where the 
        # incoming event should be place as in if the events time is before the
        # currently inspected elements time
        if (self.isEmpty() or self.items[0][1] < event[1]):
            self.items.insert(0, event)
        else:
            for i in range(len(self.items) - 1 , 0, -1):
                if (self.items[i][1] < event[1] ):
                    continue

                else:
                    self.items.insert(i, event)
                    break

        

    def enqueue(self, item):
        if len(self.items) < self.maxSize:
            self.items.append(item)
            if(item[0] == 'a'): self.numArr += 1
            elif(item[0] == 'd'): self.numArr -= 1
        else:
            print("failed to enqueue " + item)

    def dequeue(self):
        ret = self.items.pop()
        #if (ret[0] == 'a') self.numArr -= 1
        return ret

    def size(self):
        return len(self.items)

    def sort(self):
        self.items.sort(key = lambda x:-x[1]) # this is just the built-in sort
        return self.items

    def showQueue(self):
        for item in self.items:
            print(item)

if __name__ == "__main__":
    q = Queue()
    

